# ---------------------------------------------------------- #
# UI function ######
# ---------------------------------------------------------- #
initiative_module_UI <- function(id) 
{
    ns <- NS(id)
    tagList(
        #verbatimTextOutput(ns("dfTest")),
       # bs4Box(
       #     width = 12
       #     , height = "auto"
       #     , title = "test"
             uiOutput(ns("InitiativeUI"))
       # ) #end bs4Box
        # ---------------------------------------------------------- #
    ) #end tagList
} # end summary_moduleUI
# ---------------------------------------------------------- #
# SERVER function ----
# ---------------------------------------------------------- #
initiative_module <- function(input, output, session
                              , uID
                              , mainStrategyDF
                              , maindf
                              , userDF
                              , facultyCode
                              , rowClicked
                              , schoolCode
                              , kpi_TypePicker
                              , kpi_NamePicker
                              , side_SchoolSelected)
{
    # ---------------------------------------------------------- #
    ns <- session$ns
    # ---------------------------------------------------------- #
    init_table_df <- reactiveVal(NULL)
    init_table_df_temp <- reactiveVal(NULL)
    strategy_df <- reactiveVal(NULL) 
    title_name <- reactiveVal(NULL) 
    description_name <- reactiveVal(NULL) 
    faculty_short_name <- reactiveVal(NULL) 
    kpi_categoryChoice <- reactiveVal(NULL)
    kpi_nameChoice <- reactiveVal(NULL)
    update_kpi_category_choice <- reactiveVal(NULL)
    update_kpi_name_choice <- reactiveVal(NULL)
    progressPickerInit <- reactiveVal(NULL)
    progressPickerInitUP <- reactiveVal(NULL)
    main_df <- reactive(req(maindf()))
    # ---------------------------------------------------------- #
    u_id <- reactive(req(uID()))
    user_df <- reactive(req(userDF()))
    faculty_code <- reactive(req(facultyCode()))
    school_code <- reactive(req(schoolCode()))
    main_strategy_df <- reactive(req(mainStrategyDF()))
    kpiTypePicker <- reactive(kpi_TypePicker())
    kpiNamePicker <- reactive(kpi_NamePicker())
    sideSchoolSelected <- reactive(req(side_SchoolSelected()))
    # ------------------------------------------------------------- #
    # ------------------------------------------------------------- #
    observe(
    {
        #invalidateLater(3000, session)
        #init_table_df(table_data_initiative(req(rowClicked()))) 
    })
    
    observe(kpi_categoryChoice(req(input$kpi_category_choice)))
    observe(kpi_nameChoice(req(input$kpi_name_choice)))
    observe(title_name(req(input$initiative_name)))
    observe(description_name(req(input$initiative_description)))
    observe(update_kpi_category_choice(req(input$up_kpi_category_choice)))
    observe(update_kpi_name_choice(req(input$up_kpi_name_choice)))
    observe(progressPickerInit(req(input$progress_picker_init_choice)))
    observe(progressPickerInitUP(req(input$up_progress_picker_init_choice)))
    
    # ---------------------------------------------------------- #
    row_clicked_init <- reactive(unlist(req(input$init_table_DT_rows_selected), use.names = F))
    initiative_id <- reactive(initiative_subset_module(req(row_clicked_init()), req(table_df())))
    # ---------------------------------------------------------- #
    output$dfTest <- renderPrint(
      { 
        table_df()
      })
    # ---------------------------------------------------------- #
    initiative_subset_module <- function(rowclick, df)
    {
      t_list <- as.list(df$initiative_id)
      return(unlist(t_list[rowclick], use.names = F))
    }
    
    # ------------------------------------------------------------- #
    main_modul_read_initiative <- function(id_list)
    {
        df <- dbGetQuery(db, "SELECT * FROM initiatives") %>%
            filter(strategy_id %in% id_list) %>%
            filter(initiative_active == "y") %>%
            select(-date_created, -faculty_code, -initiative_active)
        return(df)
    }
    # ---------------------------------------------------------- #
    # OUTPUT'S ###########################
    # ---------------------------------------------------------- #
    output$InitiativeUI <- renderUI(
    {
        if(is.null(main_df()) | is.null(rowClicked()) )
        {
            
        }  
        else
        {
          boxPlus(
              title = paste0(user_df()$faculty_short_name ," - Initiative"),
                closable = F, 
                width = NULL,
                status = "danger", 
                solidHeader = FALSE, 
                collapsible = TRUE
                , fluidRow(DTOutput(ns('init_table_DT'), height = "auto")
                , tagList(
                     HTML("<br>")
                   , div(style="display:inline-block;width:95%;text-align:right;width:200px;", uiOutput(ns("addInitiative")))
                   , div(style="display:inline-block;width:95%;text-align:right;width:200px;", uiOutput(ns("updateInitiative")))
                   , div(style="display:inline-block;width:95%;text-align:right;width:200px;",  uiOutput(ns("deleteInitiative")))
                   , div(style="display:inline-block;width:200px;margin-left:10px; ", uiOutput(ns("plot_switch_i")))))
            , fluidRow(timevisOutput(ns("initiative_time_lines"), height = "auto"))
            )
        }
    })
    # -----------------------------------------------------------#
    output$plot_switch_i <- renderUI(
      {
        materialSwitch(inputId = ns("material_switch_i")
                       , label = "View Plots"
                       , right = F
                       , value = TRUE
                       , status = "danger")
      })
    # ---------------------------------------------------------- # 
    output$init_table_DT <- renderDT(
    {
      initiative_table(table_df())
    })
    # ---------------------------------------------------------- # 
    output$addInitiative <- renderUI(
        {
            actionButton(inputId = ns("add_initiative")
                        , label = "Add Initiative"
                       , width = "200px")
        })
    # -----------------------------------------------------------#
    output$updateInitiative <- renderUI(
        {
            actionButton(inputId = ns("select_init_button")
                        , label = "Update Initiative"
                       , width = "200px")
        })
 #------------------------------------------------------- #
    output$new_initiative_title <- renderUI(
        {
            textInput(inputId = ns("initiative_name")
                        , label = "New Initiative Name"
                        , placeholder = "Please add a Initiative Name"
                        , width = TEXT_AREA_WIDTH)
        })
    # ------------------------------------------------------------- #
    output$new_initiative_description <- renderUI(
        {
            textAreaInput(inputId = ns("initiative_description")
                          , label = "Initiative Description"
                          , placeholder = "Please add a Initiative description"
                          , width = TEXT_AREA_WIDTH
                          , height = MILESTONE_TEXT_AREA_HEIGHT)
        })
    # ------------------------------------------------------------- #
    output$strategy_picker <- renderUI(
        {
            if(is.null(rowClicked()))
            {
                strategy_df(read_strategies(req(faculty_code())))
                strategy_list <- unlist(req(strategy_df()) %>% select(strategy_name),  use.names = F)
                selectInput(inputId = ns("strategy_choice")
                            ,label = "Please Select A strategy:"
                            , choices = strategy_list
                            , selected = "Please Select a Strategy"
                            , width = "100%")  
            }
            else
            {
                strategy_df(read_strategies(req(faculty_code())))
                strategy_list <- unlist(req(strategy_df()) %>% select(strategy_name),  use.names = F)
                picked_strategy <- unlist(req(strategy_df()) %>% filter(strategy_id == req(rowClicked())) %>%  select(strategy_name),  use.names = F)
                selectInput(inputId = ns("strategy_choice")
                            ,label = "Please Select A strategy:"
                            , choices = strategy_list
                            , selected = picked_strategy
                            , width = "100%")  
            }
        })
    # ------------------------------------------------------------- #
    output$update_init_description <- renderUI(
        {
            textAreaInput(inputId = ns("update_initiative_description")
                          , label = "Update Initiative Description"
                          , value = unlist(req(main_df()) %>% filter(initiative_id == initiative_id()) %>% distinct(initiative_description), use.names = F)
                          , width = TEXT_AREA_WIDTH
                          , height = MILESTONE_TEXT_AREA_HEIGHT)
        })
    # ------------------------------------------------------------- #
    output$update_init_title <- renderUI(
        {
            textInput(inputId = ns("update_initiative_name")
                      , label = "Update Initiative Name"
                      , value = unlist(main_df() %>% filter(initiative_id == initiative_id()) %>% distinct(initiative_name), use.names = F)
                      , width = TEXT_AREA_WIDTH)
        })
    # ---------------------------------------------------------- # 
    # ---------------------------------------------------------- # 
    # OBSERVE EVENTS ###########################
    # ---------------------------------------------------------- # 
    # ---------------------------------------------------------- #
    output$kpi_category_picker <- renderUI(
        {
            kpi_category <- unlist(read_kpi_category(), use.names = F)
            kpi_category <- c("None", kpi_category)
            selectInput(inputId = ns("kpi_category_choice")
                        ,label = "Please Select a KPI Category:"
                        , choices = kpi_category
                        , selected = "None"
                        , width = "100%")  
        })
    # ---------------------------------------------------------- # 
    observeEvent(input$kpi_category_choice,
     {
        if(kpi_categoryChoice() != "None" | is.null(kpi_categoryChoice()) )
            {
             kpi_names <- unlist(read_kpi_name(kpi_categoryChoice()), use.names = F)
             output$kpi_name_picker <- renderUI(
                 { 
                   selectInput(inputId = ns("kpi_name_choice")
                             ,label = "Please Select a KPI:"
                             , choices = kpi_names
                             , selected = "Please Select a KPI"
                             , width = "100%")})
            }
         else
            {
             output$kpi_name_picker <- renderUI({ })
            }
     })
    # ---------------------------------------------------------- # 
    observeEvent(input$add_initiative,
                 {
                     showModal(initiative_load())
                 })
    # ---------------------------------------------------------- # 
    observeEvent(input$select_init_button,
                 {
                     showModal(show_init_description_modal())   
                 })
    # ---------------------------------------------------------- # 
    observeEvent(input$initiative_ok,
       {
       rValue <- write_initiative(req(title_name())
                                      , req(description_name())
                                      , req(rowClicked())
                                      , req(faculty_code())
                                      , req(school_code())
                                      , req(u_id()) 
                                      , req(kpi_categoryChoice())
                                      , req(kpi_nameChoice())
                                      , req(progressPickerInit())
                                  )
        update_initiative_position_counter(req(faculty_code()))
        if_else(rValue == T, removeModal(), NULL)
       })
    # ---------------------------------------------------------- # 
    #update_initiative_position_counter("ALE")
    # ---------------------------------------------------------- # 
    observeEvent(input$update_init_description_ok,
       {
       rValue <- updateInitiative_details(req(initiative_id()) ## need to sort
                                          , req(faculty_code())
                                          , req(input$update_initiative_name)
                                          , req(input$update_initiative_description)
                                          , req(update_kpi_category_choice())
                                          , req(update_kpi_name_choice())
                                          , req(progressPickerInitUP()))
       update_initiative_position_counter(req(faculty_code()))
       if_else(rValue == T, removeModal(), NULL)
       
    })
    # ---------------------------------------------------------- #

    output$delete_initiative_string <- renderUI(
        { 
            HTML(paste0("Do you wish to delete ", initiative_id(),"? <br> It will remove all Milestones attached to this Initiative"))
        })
    # ---------------------------------------------------------- #
    # ---------------------------------------------------------- # 
    output$deleteInitiative <- renderUI(
        {
            actionButton(inputId = ns("delete_initiative")
                         , label = "Delete Initiative"
                         , width = "200px")
        })
    # ---------------------------------------------------------- # 
    observeEvent(input$delete_initiative,
                 {
                     if(is.null(initiative_id()))
                     {
                         
                     }
                     else
                     {
                         showModal(init_delete())
                     }
                 })
    # ---------------------------------------------------------- # 
    # FUNCTIONS ###########################
    # ---------------------------------------------------------- # 
    init_delete <- function(failed = FALSE) 
    {
        ns <- session$ns
        modalDialog(
            size = "l"
            , easyClose = F
            , fade = T
            , title = h5(paste0(user_df()$faculty_short_name, " - Delete Initiative"))
            , h5(htmlOutput(ns("delete_initiative_string")))
            #, uiOutput("new_strategy_description")
            , footer = tagList(
                modalButton("Cancel"),
                actionButton(ns("initiative_del"), "DELETE")))
    }
    # ------------------------------------------------------------- #
    observeEvent(input$initiative_del,
     {
         rValue <- delete_initiatives_function(req(faculty_code()), req(initiative_id()))
         if_else(rValue == T, removeModal(), NULL)
     })
    # ---------------------------------------------------------- #
    delete_initiatives_function <- function(f_id, init_id)
    { 
        marker <- "n"
        
        init_df <- dbGetQuery(db, "SELECT * FROM initiatives") %>%
            filter(initiative_id  == init_id) %>%
            filter(faculty_code == f_id) %>%
            filter(initiative_active == "y")
        
        initCount <- nrow(init_df)
        
        mile_df <- dbGetQuery(db, "SELECT * FROM milestones") %>%
            filter(initiative_id == init_id) %>%
            #filter(faculty_code == f_id) %>%
            filter(milestone_active == "y")
        
        mileCount <- nrow(mile_df)
        print(paste0("initCount: ", initCount))
        print(paste0("mileCount: ", mileCount))
        if(mileCount == 0 & initCount > 0)
        {
            sql <- glue_sql("UPDATE initiatives
                    SET initiatives.initiative_active = {marker}
                    WHERE initiatives.initiative_id = {init_id} AND initiatives.faculty_code = {f_id};", .con = db)
            return(DBI::dbExecute(db, sql))
        }
        else if(mileCount > 0 & initCount > 0)
        {
            sql <- glue_sql("UPDATE milestones
                    SET milestones.milestone_active = {marker}
                    WHERE milestones.initiative_id = {init_id};", .con = db)
            
            DBI::dbExecute(db, sql)
            
            
            sql2 <- glue_sql("UPDATE initiatives
                    SET initiatives.initiative_active = {marker}
                    WHERE initiatives.initiative_id = {init_id} AND initiatives.faculty_code = {f_id};", .con = db)
                            
            return(DBI::dbExecute(db, sql2))
        }
    }
    # ---------------------------------------------------------- #
    updateInitiative_details <- function(i_id, f_id, update_Name, updateDescription_name, updateCategory_choice, updateName_choice, initiativeChoice)
    {
        print(paste0("i_id: ", i_id))
        print(paste0("f_id: ", f_id))
        print(paste0("update_Name: ", update_Name))
        print(paste0("updateDescription_name: ", updateDescription_name))
        print(paste0("updateCategory_choice: ", updateCategory_choice))
        print(paste0("updateName_choice: ", updateName_choice))
        print(paste0("initiativeChoice: ", initiativeChoice))
        
        sql <- glue_sql("UPDATE initiatives
                    SET initiatives.initiative_name = {update_Name}
                    , initiatives.initiative_description = {updateDescription_name}
                    , initiatives.kpi_category = {updateCategory_choice}
                    , initiatives.kpi_name = {updateName_choice}
                    , initiatives.initiative_choice = {initiativeChoice} 
                    WHERE initiatives.initiative_id = {i_id} AND initiatives.faculty_code = {f_id};" , .con = db)
        return(DBI::dbExecute(db, sql))
    }
    # ---------------------------------------------------------- #
    initiative_load <- function(failed = FALSE) 
    {
        ns <- session$ns
        modalDialog(
            size = "m"
            , easyClose = F
            , fade = T
            , title = h5(paste0(user_df()$faculty_short_name, " - Add Initiative"))
            , uiOutput(ns("strategy_picker"))
            , uiOutput(ns("new_initiative_title"))
            , uiOutput(ns("new_initiative_description"))
            , uiOutput(ns("progress_picker_init"))
            , uiOutput(ns("kpi_category_picker"))
            #, uiOutput(ns("kpi_type_picker"))
            , uiOutput(ns("kpi_name_picker"))
            , footer = tagList(
                modalButton("Cancel"),
                actionButton(ns("initiative_ok"), "OK")))
    }
    # ---------------------------------------------------------- # 
    write_initiative <- function(in_name, in_description,  s_id, f_id, school_code, u_id, categoryChoice, name_choice, progressPickerInit)
    {
        df <- data.frame(
            initiative_id = round(runif(1, 1, 90000000),0)
            , initiative_name = in_name
            , initiative_description = in_description
            , strategy_id = s_id
            , faculty_code = f_id
            , school_code = school_code
            , initiative_active = "y"
           # , date_created = Sys.Date()
            , date_created = Sys.time()
            , user_id = u_id
            , kpi_category = categoryChoice
            , kpi_name = name_choice
            , initiative_choice = progressPickerInit) %>% 
            mutate_if(is.factor, as.character)
        
        rs <- dbWriteTable(db, "initiatives", df, append = T, overwrite = F)
        return(rs)
    }
    #write_initiative("in_name", "in_description",  56565, "f_id", "school_code", 565656, "categoryChoice", "name_choice")
    # ------------------------------------------------------------- #
    update_initiative_position_counter <- function(f_id)
    {
      sql <- glue_sql("UPDATE a
                        SET a.position_i = b.position_i
                        FROM initiatives a
                        INNER JOIN
                        (
                          SELECT 
                          initiative_id, date_created,
                          DENSE_RANK() OVER (ORDER BY date_created) AS position_i
                          FROM initiatives
                          WHERE initiative_active = 'y' AND faculty_code = {f_id}
                        ) b
                        ON a.initiative_id = b.initiative_id", .con = db)
      
      return(DBI::dbExecute(db, sql))
    }
    #update_initiative_position_counter("ALE")
    # ------------------------------------------------------------- #
    # ------------------------------------------------------------- #
    output$up_kpi_category_picker <- renderUI(
        {
            kpi_category <- unlist(read_kpi_category(), use.names = F)
            kpi_category <- c("None", kpi_category)
            selectInput(inputId = ns("up_kpi_category_choice")
                        ,label = "Please Select a KPI Category:"
                        , choices = kpi_category
                        , selected = "None"
                        , width = "100%")  
        })
    # ---------------------------------------------------------- #
    # ------------------------------------------------------------- #
    observeEvent(input$up_kpi_category_choice,
       {
           if(update_kpi_category_choice() != "None" | is.null(update_kpi_category_choice()) )
           {
               kpi_names <- unlist(read_kpi_name(update_kpi_category_choice()), use.names = F)
               output$up_kpi_name_picker <- renderUI(
                   { 
                   selectInput(inputId = ns("up_kpi_name_choice")
                               ,label = "Please Select a KPI:"
                               , choices = kpi_names
                               , selected = "Please Select a KPI"
                               , width = "100%")})
           }
           else
           {
               output$up_kpi_name_picker <- renderUI({ })
           }
       })
    # ------------------------------------------------------------- #
    output$progress_picker_init <- renderUI(
      {
        selectInput(inputId = ns("progress_picker_init_choice")
                    ,label ="Please Select The Initiative Progress Status:"
                    , choices = INIT_PROGRESS_STATUS
                    , selected = INIT_PROGRESS_STATUS[4]
                    , width = "100%")
      })
    # ------------------------------------------------------------- #
    output$up_progress_picker_init <- renderUI(
      {
        selectInput(inputId = ns("up_progress_picker_init_choice")
                    ,label ="Please Select The Initiative Progress Status:"
                    , choices = INIT_PROGRESS_STATUS
                    , selected = INIT_PROGRESS_STATUS[4]
                    , width = "100%")
      })
    # ------------------------------------------------------------- #
    show_init_description_modal <- function(failed = FALSE) 
    {
        modalDialog(
            size = "m"
            , easyClose = F
            , fade = T
            , title = h5(paste0(user_df()$faculty_short_name, " - Update Initiative"))
            , uiOutput(ns("update_init_title"))
            , uiOutput(ns("update_init_description"))
            , uiOutput(ns("up_progress_picker_init"))
            , uiOutput(ns("up_kpi_category_picker"))
            , uiOutput(ns("up_kpi_name_picker"))
            , footer = tagList(
                modalButton("Cancel"),
                actionButton(ns("update_init_description_ok"), "UPDATE")))
    }
    # ------------------------------------------------------------- #
    # ------------------------------------------------------------- #
    # ---------------------------------------------------------- #
    status_df <- reactive(
      {
        req(main_df()) %>%
          filter(strategy_id == rowClicked()) %>% 
          group_by(initiative_name, milestone_progress_status) %>%
          summarise(count = n()) %>% 
          mutate(score = case_when(
            milestone_progress_status == MILESTONE_PROGRESS_STATUS[2] ~ count* MILESTONE_PROGRESS_SCORE[2]
            , milestone_progress_status == MILESTONE_PROGRESS_STATUS[3] ~ count* MILESTONE_PROGRESS_SCORE[3]
            , milestone_progress_status == MILESTONE_PROGRESS_STATUS[4] ~ count* MILESTONE_PROGRESS_SCORE[4]
            , milestone_progress_status == MILESTONE_PROGRESS_STATUS[5] ~ count* MILESTONE_PROGRESS_SCORE[5])) %>% 
          ungroup() %>%
          group_by(initiative_name) %>%
          summarise(score = sum(score, na.rm = T), count = sum(count, na.rm = T), final_score = score/count) %>% 
          mutate(colour_marker = case_when(
            final_score > 99 ~ MILESTONE_PROGRESS_STATUS[2]
            , final_score > 0 & final_score < 100 ~ MILESTONE_PROGRESS_STATUS[3]
            , final_score > -10 & final_score < 0 ~ MILESTONE_PROGRESS_STATUS[4]
            , final_score < -9 ~ MILESTONE_PROGRESS_STATUS[5]
          ))
      })
    # ---------------------------------------------------------- #
    df <- reactive(
      {
        return(
          req(main_df()) %>%
            filter(strategy_id == rowClicked()) %>% 
            group_by(initiative_name) %>%
            summarise(start_date = min(start_date), end_date = max(end_date)) %>%
            left_join(status_df(), by = "initiative_name") %>% 
            mutate(style =
                     case_when(
                         colour_marker == MILESTONE_PROGRESS_STATUS[1] ~ paste0("color: #fff; background-color:", STATUS_1,"border-color: #191818;", DROP_SHADOW_TEXT)
                       , colour_marker == MILESTONE_PROGRESS_STATUS[2] ~ paste0("color: #fff; background-color:", STATUS_2,"border-color: #191818;", DROP_SHADOW_TEXT)
                       , colour_marker == MILESTONE_PROGRESS_STATUS[3] ~ paste0("color: #fff; background-color:", STATUS_3,"border-color: #191818;", DROP_SHADOW_TEXT)
                       , colour_marker == MILESTONE_PROGRESS_STATUS[4] ~ paste0("color: #fff; background-color:", STATUS_4,"border-color: #191818;", DROP_SHADOW_TEXT)
                       , colour_marker == MILESTONE_PROGRESS_STATUS[5] ~ paste0("color: #fff; background-color:", STATUS_5,"border-color: #191818;", DROP_SHADOW_TEXT))) %>%
            distinct(initiative_name, style, start_date, end_date))
      })
    # ---------------------------------------------------------- #
    group_time_line <- reactive(
      {
        return(
          data.frame(
            id = 1:nrow(df())
            , start = unlist(df()$start_date, use.names = F)
            , end = unlist(df()$end_date, use.names = F)
            , content = unlist(df()$initiative_name, use.names = F)
            , style = unlist(df()$style, use.names = F)))
      })
    # ---------------------------------------------------------- #
    output$initiative_time_lines <- renderTimevis(
      {
        if(req(input$material_switch_i) == T)
        {
          validate(need(!is_empty(group_time_line), 'No Timeline Data...'))
          timevis(data = group_time_line()
                  , fit = T
                  , options = list(stack = T))
        }
        
      })
    
    # ---------------------------------------------------------- #
    table_df <- reactive(
      {
        marker <- main_df() %>% filter(strategy_id == rowClicked() & initiative_active =="y") 
        if(nrow(marker) > 0)
        {
          return(main_df() %>% 
                    filter(strategy_id == rowClicked()) %>% 
                    filter(initiative_active =="y") %>% 
                    group_by(initiative_id, initiative_name, initiative_description, school_code, kpi_category, kpi_name, initiative_choice, position_s, position_i) %>% 
                    summarise(start_date = min(start_date), end_date = max(end_date), days_remaining = max(days_remaining), time_frame = max(time_frame)) %>% 
                    #mutate(position_s = as.integer(position_s)) %>% 
                    arrange(position_s, position_i) %>%
                    ungroup() %>% 
                    mutate(initiative_name = paste0(user_df()$faculty_short_name, "_S", position_s, "_I", position_i)) %>% 
                    ungroup())
        }
      })
    # ---------------------------------------------------------- #
    initiative_table <- function(df)
    {
     df <- df %>% left_join(status_df() %>% 
                              select(initiative_name, colour_marker), by="initiative_name") %>% 
                              mutate(colour_marker = str_replace(colour_marker, "Milestone", "Initiative")) %>% 
                              ungroup() %>% 
                              select(initiative_id #0
                                     #, "Naming Rule" = naming_rule
                                     , "Initiative Name" = initiative_name
                                     , "Initiative Description" = initiative_description
                                     , "Start Date" = start_date
                                     , "End Date" = end_date
                                     , school_code #6
                                     , "KPI Category" = kpi_category
                                     , "KPI Name" = kpi_name
                                     , "Days Remaining" = days_remaining
                                     , "Time Frame" = time_frame
                                     #, "Percent Expired" = percent_expired 
                                     , "Initiative Choice" = initiative_choice
                                     , "Initiative Status" =  colour_marker)
     
        dt <- DT::datatable(df,
                    escape = F,
                    extensions = c('ColReorder'),
                    rownames = FALSE,
                    selection = list(mode = "single", target = "row"),
                    options = list(
                        dom = 't',
                        colReorder = list(realtime = T),
                        columnDefs = list(list(className = 'dt-left', targets="_all"), list(visible=FALSE, targets=c(0,6))),
                        pageLength = 100,
                        paging = F,
                        searching = F)) 
          return(dt)
    }
    # ------------------------------------------------------------- #
    # ------------------------------------------------------------- #
    init_button_loader <- function(FUN, id, ...)
    {
        return(as.character(FUN(paste0(id), ...)))
    }
    # ------------------------------------------------------------- #
    # ------------------------------------------------------------- #
    return(initiative_id)
} # END MODULE #####################
# ------------------------------------------------------------- #    
# ------------------------------------------------------------- #
# ------------------------------------------------------------- #


