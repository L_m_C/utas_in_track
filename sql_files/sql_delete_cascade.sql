ALTER TABLE strategy
    CONSTRAINT fk_strat_initiative_id
    FOREIGN KEY (strategy_id)
    REFERENCES initiatives (strategy_id)
    ON DELETE CASCADE;


ALTER TABLE initiatives
    CONSTRAINT fk_init_initiative_id
    FOREIGN KEY (strategy_id)
    REFERENCES strategy (strategy_id)
    ON DELETE CASCADE;




CREATE TABLE initiatives_Temp (
  initiative_id INT PRIMARY KEY,
  initiative_name VARCHAR(120),
  initiative_description VARCHAR(255) NOT NULL,
  strategy_id INT NOT NULL,
  college_id INT NOT NULL,
  initiative_active VARCHAR(1) NOT NULL,
  date_created DATETIME NOT NULL,
  PRIMARY KEY (initiative_id)
  CONSTRAINT fk_init_initiative_id
    FOREIGN KEY (strategy_id)
    REFERENCES strategy (strategy_id)
    ON DELETE CASCADE);

ALTER TABLE milestones
    CONSTRAINT fk_mile_initiative_id
    FOREIGN KEY (initiative_id)
    REFERENCES initiatives (initiative_id)
    ON DELETE CASCADE;